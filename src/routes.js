import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Home from './pages/home';
import Page1 from './pages/page1';
import Page2 from './pages/page2';
import Page3 from './pages/page3';
import Page4 from './pages/page4';

function Routes() {
  return (
      <BrowserRouter>
        <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/pagina1" component={Page1} />
        <Route path="/pagina2" component={Page2} />
        <Route path="/pagina3" component={Page3} />
        <Route path="/pagina4" component={Page4} />
        </Switch>
      </BrowserRouter>
        
	
  );
}

export default Routes;
