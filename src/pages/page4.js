import React from 'react';
import { Image } from 'react-bootstrap';

function Page4() {
  return (
    <div>
    <center>
    <h2>Tá, mas e daí?</h2>
      <p>Em que minha opinião importa? Vocẽ está certa: em nada <br/>
      É vocẽ que tem ver sua beleza, tanto externa quando interna <br/>
      Agora olhe pra essa foto e me diga: Você realmente se acha feia?
      </p>
      <Image className="image" src="/assets/IMG-20200314-WA0009.jpg" roundedCircle />
      <p>Se a resposta for sim, eu vou te bater kkkk <br/>
      Mesmo que você ainda não ache, saiba que você é uma pessoa <br/>
      Muito especial para mim, e para todos seus outros amigos <br/>
      e eu não queria que vocẽ ficasse triste por um segundo se quer <br/>
      Eu realmente sou grato pela nossa amizade, e por tudo que vocẽ me proporcionou, mesmo eu levando um fora kkk <br/>
      Desejo tudo de bom, e uma ótima noite (Desculpe por deixar vocẽ curiosa, e talvez desapontada, mas foi de coração)
      
      </p>
    </center>
      
    </div>
  );
}

export default Page4;