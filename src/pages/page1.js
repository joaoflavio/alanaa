import React from 'react';
import { Alert, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
function Page1() {
  return (
    <center>
      <h2>Prólogo</h2>
      <p className="text-secoundary">Resolvi fazer algo diferente aqui,<br /> só pra dar uma diferenciada.
      Ontem você me disse que estava<br /> triste, com autoestima baixa e se sentindo feia. Espero que isso te
      anime um pouco.<br /><br />
      <Alert className="mw" variant='primary'>PS: Pode conter alto teor de Tumor e sinceridade</Alert><br />
      <Link to="/pagina2"><Button>Próxima</Button></Link>
      </p>
    </center>
    );
}

export default Page1;