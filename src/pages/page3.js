import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Page3() {
  return (

    <div>
    <center>
    <h2>Ainda não acredita?</h2>
    <p>Segue abaixo uma lista de qualidades que eu enxergo em você</p>
    <Table className="mw" striped bordered hover size="sm" variant="dark">
      <thead>
        <tr>
          <th>#</th>
          <th>Qualidade</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>Bonita</td>
        </tr>
        <tr>
          <td>2</td>
          <td>Inteligente</td>
        </tr>
        <tr>
          <td>1</td>
          <td>Sarcástica e bem humorada</td>
        </tr>
        <tr>
          <td>3</td>
          <td>Corpo bonito</td>
        </tr>
        <tr>
          <td>4</td>
          <td>Atenciosa</td>
        </tr>
        <tr>
          <td>5</td>
          <td>Minha amiga (Ótima qualidade)</td>
        </tr>
      </tbody>
    </Table>
    <p>Tá vendo? Você é uma ótima maravilhosa. Namora comigo kkkk</p>
    <Link to="/pagina4">
    <Button>Próxima</Button>
  </Link>
    </center>
    
    
    </div>
    
  );
}

export default Page3;