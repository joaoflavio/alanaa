import React from 'react';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom'

function Home() {
  return (
    <center>
    <div>
    <h1>Bem vinda Alana</h1>
  <p>
    Feito com <FontAwesomeIcon icon={faHeart} /> por João Flávio
  </p>
  <p>
    <Link to="/pagina1">
    <Button variant="primary">Iniciar</Button>
    </Link>
  </p>
  </div>
    </center>
    
  
  );
}

export default Home;