import React from 'react';
import { Image, Alert, Container, Row, Button, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
function Page2() {
  return (
    <div>
    <center>
    <h2>Fotos</h2>
    <p>Bem, na maioria das fotos eu não respondi a altura <br/>
    Porém agora irei dizer exatamente o que eu achei de cada uma<br/>
    </p>
    <Alert className="mw" variant="primary">Se eu falar merda, perdoe kkk</Alert>
    </center>
     
      <Container>
      <Row>
      <Col xs={6} md={4}>
      <p>O que falar dessa aqui? Uma bela moça dançando com um nobre rapaz... <br/>
        Gostei muito de dançar com você, apesar da vergonha, e espero que possamos<br/>
        Fazer isso na próxima festa junina que a escola tiver. Topa?
        </p>
        <Image className="image" src="/assets/IMG-20191221-WA0003.jpg" thumbnail />
      </Col>
      <Col xs={6} md={4}>
      <p>Esse foi o dia que você estava saindo com a Elisa e me mandou essa foto <br/>
        Podemos notar uma grande ajuda da câmera que tirou ela e o efeito que o sol causou.<br/>
        Resumo: Foto bonita com uma moça bonita. Combinação perfeita. Aprovado
        </p>
        <Image className="image" src="/assets/IMG-20200314-WA0009.jpg" thumbnail />
      </Col>
      <Col xs={6} md={4}>
      <p>Aqui você estava testando um filtro do Instagram <br/>
        Eu particularmente gostei bastante da foto. Bonita<br/>
        Seu sorriso ficou foto e se destacou. Parabéns
        </p>
        <Image className="image" src="/assets/IMG-20200321-WA0027.jpg" thumbnail />
      </Col>
      <Col xs={7} md={4}>
      <p>Essa foi um novo estilo de foto pra mim. <br/>
        Você sempre tira foto sorrindo, mas nessa vocẽ está com um ar sério<br/>
        Ficou melhor do que eu imaginava. Está realmente linda nessa
        </p>
        <Image className="image" src="/assets/IMG-20200322-WA0009.jpg" thumbnail />
      </Col>

      <Col xs={7} md={4}>
      <p>Essa você mandou logo em seguida, e voltou ao "normal"<br/>
        Seu sorriso sempre me encanta por ser bonito<br/>
        E dessa vez vocẽ estava com um batom, né?
        </p>
        <Image className="image" src="/assets/IMG-20200322-WA0010.jpg" thumbnail />
      </Col>

      <Col xs={7} md={4}>
      <p>Eu lembro perfeitamente desse dia <br/>
        Eu já tinha ido dormir, e parecia que na noite anterior a conserva não tinha fluido<br/>
        E você mandou uma sequência de fotos, e no dia seguinte eu fiquei felizão kkk.
        </p>
        <Image className="image" src="/assets/IMG-20200324-WA0003.jpg" thumbnail />
      </Col>
      <Col xs={8} md={4}>
      <p>Nesse dia você tava me "ensinando" a fazer maquiagem kkk <br/>
        Eu descobri que, de qualquer jeito vocẽ fica bonita. Naturalmente ou não<br/>
        Fazer o que né? Genética boa é foda
        </p>
        <Image className="image" src="/assets/IMG-20200329-WA0009.jpg" thumbnail />
      </Col>
      <Col xs={8} md={4}>
      <p>Puxa, essa você vai me perdoar mas... <br/>
        Essa fez o geraldo subir, se é que me entende kkkkkk<br/>
        É, ficou MUITO PERFEITA. Me desculpe, mas é isso
        </p>
        <Image className="image" src="/assets/IMG-20200401-WA0026.jpg" thumbnail />
      </Col>
      <Col xs={8} md={4}>
      <p>Esse dia foi um dejavu enormeee <br/>
        Eu fiquei analizando muito tempo ela, eu sabia que reconhecia esse vestido<br/>
        Lembrei daquela nossa noite de dança. Você estava maravilhosa com esse vestido
        </p>
        <Image className="image" src="/assets/IMG-20200406-WA0007.jpg" thumbnail />
      </Col>
        
      </Row>
    </Container>
    <center>
      <Link to="/pagina3">
        <Button>Próxima</Button>
      </Link>
    </center>
    </div>
    
    );
}

export default Page2;